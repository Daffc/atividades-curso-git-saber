#include <stdio.h>
#include <stdlib.h>

long long fatorial(long long n)
{
  for (unsigned int i = n - 1; i > 1; i--)
    n *= i;

  return n;
}

int main(int argc, char **argv, char **envp)
{
	if(argc > 1)
	{
		printf("%lld\n", fatorial(atoll(argv[1])));
	}
	else
	{
		puts("Voce precisa dar um valor para n!");
	}
}
